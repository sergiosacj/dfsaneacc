# Accelerated Sequential Residual Method for Nonlinear Systems

This is `BBdfsaneacc`, an [R](https://www.r-project.org/) package for solving
large-scale nonlinear systems of equations using derivative-free accelerated
sequential residual method. The code is a merge between the dfsane
implementation by Ravi Varadhan and the accelerated dfsane implementation by
Birgin, Gardenghi, Marcondes and Martínez.

This repository includes the following source files:

* the folder `cutest-R-interface` includes an interface to use
  [CUTEst](https://github.com/ralna/CUTEst/wiki) nonlinear systems without
  derivatives in R. It is the same version provided by Birgin, Gardenghi,
  Marcondes and Martínez.

* the folder `dfsaneacc-R-package` includes the R package
  `dfsaneacc`. For more details on installation in R, see the
  following section.

* the folder `paper-experiments` includes all the source files necessary to
  reproduce the results of the comparison between `dfsaneacc-R-package` and
  the implementation of DF-SANE, made by Ravi Varadhan, in the BB package.

## How to install and use the package in R

### How to install

You can run an R session and import the package after cloning this repository.
```
> library("devtools")
> load_all('./dfsaneacc-R-package')
```

### How to use dfsaneacc

You must call the routine
```
dfsaneacc <- function (par, fn, method=2, control=list(), nhlim=6, ndiis=1, alertConvergence=TRUE, acc=TRUE, ...)
```
There are some comments in the file `dfsaneacc.R` that explains the arguments and the output.

### Details

Convergence of the algorithm is declared when the L2-norm is less or
equal than `tol`. The default value for `tol` is `1.0e-7`.

The algorithm employs the function `fn` to compute the value of the
nonlinear system at a given point `par`. The function `fn` must have
the form `fn (par, ...)`.

### Example

To solve the Exponential Function 2 (see La Cruz and Raydan, 2003):

```
n <- 3
x0 <- rep(1/n^2, n)

expfun2 <- function(x) {
    n <- length(x)
    f <- rep(NA, n)
    f[1] <- exp(x[1]) - 1.0
    f[2:n] <- (2:n)/10.0 * (exp(x[2:n]) + x[1:n-1] - 1)
    f
}

ret <- dfsaneacc(par = x0, fn = expfun2, control = list(trace = FALSE))
ret
```

## The CUTEst interface

This interface consists in a set of R routines to evaluate the
objective function of the nonlinear system problems from CUTEst
testset.

For more details check `https://github.com/johngardenghi/dfsaneacc/`.

### How to use the interface

For example, to solve `BOOTH` problem using this interface:

```
library('devtools')
load_all('./dfsaneacc-R-package')
source('./cutest-R-interface/cutest.R')

cutest_init('BOOTH', interfaceDir = "./cutest-R-interface")

n <- cutest_getn()
x0 <- cutest_getx0()
ret <- dfsaneacc(par=x0, fn=cutest_evalr, control = list(tol=1.0e-6*sqrt(n), trace = FALSE))

cutest_end()
```
