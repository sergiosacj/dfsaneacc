#!/bin/bash

rg "Problem name" > /tmp/problem.txt
rg "Residual Euclidian norm" > /tmp/norm.txt
rg "Number of functional evaluations" > /tmp/nfeval.txt
rg "Number of iterations" > /tmp/niter.txt
rg "CPU time in seconds" > /tmp/cpu.txt
rg "Satisfied stopping criterion" > /tmp/stop_criteria.txt
