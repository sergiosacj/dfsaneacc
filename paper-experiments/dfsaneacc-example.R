library("devtools")
load_all('../dfsaneacc-R-package')

n <- 3
x0 <- rep(1 / n^2, n)

expfun2 <- function(x) {
  n <- length(x)
  f <- rep(NA, n)
  f[1] <- exp(x[1]) - 1.0
  f[2:n] <- (2:n) / 10.0 * (exp(x[2:n]) + x[1:n - 1] - 1)
  f
}

ret <- dfsaneacc(par = x0, fn = expfun2, control = list(trace = FALSE))

ret
