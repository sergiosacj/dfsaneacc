using BenchmarkProfiles
using Plots

data = [
[         8    5.0];
[        42     24];
[        26     35];
[        17     13];
[        40     21];
[         8      5];
[        11     11];
[       211    221];
[        15     11];
[        14     11];
[       367    208];
[        16     11];
[        39     24];
[       564    766];
[      3466     28];
[1420604632 315631];
[        57     29];
[        27      8];
[        64     69];
[       113   1942];
[         6      7];
[         8     11];
[        16     15];
[        12     17];
[       545    754];
[       483    233];
];
performance_profile(PlotsBackend(), data, ["DF-SANE", "DF-SANEacc"], title="Avaliações de função")
# x = 0
# 0.38461538461538464
# 0.6538461538461539

data = [
[        7    2.0];
[       40     11];
[       24     16];
[       16      6];
[       27      7];
[        7      2];
[       10      5];
[      188    191];
[       14      5];
[       13      5];
[      106     28];
[       15      5];
[       37     11];
[      428    339];
[      785     11];
[ 81301108  37854];
[       56     11];
[       25      3];
[       61     34];
[      100    220];
[        5      3];
[        7      5];
[       15      7];
[        9      7];
[      112    161];
[      102    101];
];
performance_profile(PlotsBackend(), data, ["DF-SANE", "DF-SANEacc"], title="Quantidade de iterações")
# x = 0
# 0.11538461538461539
# 0.8846153846153846

data = [
[    0.009677   0.071090];
[    0.012481   0.065467];
[    0.010722   0.068262];
[    0.010610   0.067065];
[    0.012112   0.074197];
[    0.008975   0.065403];
[    0.009745   0.067633];
[    0.019576   0.095881];
[    0.009763   0.073121];
[    0.009430   0.070058];
[    0.022314   0.080151];
[    0.010608   0.067344];
[    0.011259   0.071052];
[    0.030767   0.150036];
[    0.121268   0.067344];
[18139.287895  10.716876];
[    0.014227   0.072016];
[    0.010080   0.065017];
[    0.013931   0.072839];
[    0.015156   0.159600];
[    0.009550   0.070150];
[    0.016313   0.074277];
[    0.007080   0.061283];
[    0.031289   0.096375];
[    1.603910   2.246373];
[    0.203326   0.406652];
];
performance_profile(PlotsBackend(), data, ["DF-SANE", "DF-SANEacc"], title="Tempo de CPU (segundos)")
