import pandas as pd

dfsane_output = './outputs/dfsane/dfsane_results.csv'
dfsaneacc_output = './outputs/dfsaneacc/dfsaneacc_results.csv'
error = './outputs/tolerance.txt'

columns = ['problem', 'norm', 'nfeval', 'niter', 'cpu', 'stop']

dataframe_dfsane = pd.read_csv(dfsane_output, names=columns, skiprows=1)
dataframe_dfsaneacc = pd.read_csv(dfsaneacc_output, names=columns, skiprows=1)
dataframe_error = pd.read_csv(error, names=['problem', 'error'], skiprows=1)

df = pd.merge(dataframe_dfsane, dataframe_dfsaneacc, on=['problem'], suffixes=('_dfsane', '_dfsaneacc'))
df = df.merge(dataframe_error, on=['problem'])

# check how many problems dfsane solved
print('=== DFSANE ===')
count = 0
dfsane_solved = []
for index, row in df.iterrows():
    problem = row['problem']
    fm = row['norm_dfsane']
    fmin = min(fm, row['norm_dfsaneacc'])
    e = row['error']
    condition = (fm - fmin) / max(1, abs(fmin))
    if condition <= e:
        count+=1
        dfsane_solved.append({ 'problem': problem, 'solved': True })
    else:
        dfsane_solved.append({ 'problem': problem, 'solved': False })
print(f"dfsane solved {count} problems.\n")
dfsane_solved = pd.DataFrame(dfsane_solved)

# check how many problems dfsaneacc solved
print('=== DFSANEacc ===')
count = 0
solved = []
for index, row in df.iterrows():
    problem = row['problem']
    fm = row['norm_dfsaneacc']
    fmin = min(fm, row['norm_dfsane'])
    e = row['error']
    condition = (fm - fmin) / max(1, abs(fmin))
    if condition <= e:
        count+=1
        solved.append({ 'problem': problem, 'solved': True })
    else:
        solved.append({ 'problem': problem, 'solved': False })
print(f"dfsaneacc solved {count} problems.\n")
df = pd.merge(dfsane_solved, pd.DataFrame(solved), on=['problem'], suffixes=('_dfsane', '_dfsaneacc'))

print(df.to_string())
