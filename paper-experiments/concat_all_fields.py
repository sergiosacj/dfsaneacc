with open('problem_name.txt') as f:
    problem_name = f.read().strip().split('\n')

with open('norm.txt') as f:
    norm = f.read().strip().split('\n')

with open('nfeval.txt') as f:
    nfeval = f.read().strip().split('\n')

with open('niter.txt') as f:
    niter = f.read().strip().split('\n')

with open('cpu.txt') as f:
    cpu = f.read().strip().split('\n')

with open('stop_criteria.txt') as f:
    stop_criteria = f.read().strip().split('\n')

# Combine the information
combined_data = zip(problem_name, norm, nfeval, niter, cpu, stop_criteria)

# Print or process the combined data as needed
for data in combined_data:
    print(','.join(data))
