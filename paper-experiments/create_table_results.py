import pandas as pd

dfsane_output = './outputs/dfsane/dfsane_results.csv'
dfsaneacc_output = './outputs/dfsaneacc/dfsaneacc_results.csv'
error = './outputs/error.txt'

columns = ['problem', 'norm', 'nfeval', 'niter', 'cpu', 'stop']

dataframe_dfsane = pd.read_csv(dfsane_output, names=columns, skiprows=1)
dataframe_dfsaneacc = pd.read_csv(dfsaneacc_output, names=columns, skiprows=1)
dataframe_error = pd.read_csv(error, names=['problem', 'error'], skiprows=1)

df = pd.merge(dataframe_dfsane, dataframe_dfsaneacc, on=['problem'], suffixes=('_dfsane', '_dfsaneacc'))
df = df[['problem', 'norm_dfsane', 'norm_dfsaneacc', 'nfeval_dfsane', 'nfeval_dfsaneacc', 'niter_dfsane', 'niter_dfsaneacc', 'cpu_dfsane', 'cpu_dfsaneacc', 'stop_dfsane', 'stop_dfsaneacc']]

print(df.to_string())
